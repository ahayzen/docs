

# Install

```
sudo apt install lxd
```

For ZFS, follow the install section of the [ZFS guide](/services/zfs.md)

If you are not restarting use the following command to add your user to the group.

```
newgrp lxd
```

# Setup

```
sudo lxd init
```

Run through the following setup:

```
Name of the storage backend to use (dir or zfs) [default=zfs]: zfs
Create a new ZFS pool (yes/no) [default=yes]? yes
Name of the new ZFS pool [default=lxd]: LXDPool
Would you like to use an existing block device (yes/no) [default=no]? no
Size in GB of the new loop device (1GB minimum) [default=86]: 80
Would you like LXD to be available over the network (yes/no) [default=no]? no
Do you want to configure the LXD bridge (yes/no) [default=yes]? yes
```

Answer yes to the networking dialogs, select ipv4/6 subnets.

Run lxc to generate a client certificate.

```
lxc list
```

# Create container

```
lxc launch ubuntu:16.04 xenial-container
```

