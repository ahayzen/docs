
This installation guide assumes you have run the following installs beforehand.

  * [LXD](/virtual/lxd.md)

Environment

The name of the base wagtail container and the name of the site container.

```
NAME=wagtail
SITE_NAME=wagtail-site
```

# Create container for wagtail

Create a new ubuntu xenial container

```
lxc launch ubuntu:xenial $NAME
lxc exec $NAME -- apt update
```

# Wagtail setup

Install pip3 in the container

```
lxc exec $NAME -- apt -y install python3-pip
```

Wagtail apt requirements

```
lxc exec $NAME -- apt -y install libtiff5-dev libjpeg8-dev zlib1g-dev \
    libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python-tk
```

Install wagtail

```
lxc exec $NAME -- su - ubuntu -c "pip3 install wagtail"
```

Add aliases for dj and djrun

```
lxc exec $NAME -- su - ubuntu -c "echo \"alias dj=\\\"python3 /home/ubuntu/src/manage.py\\\"
alias djrun=\\\"dj runserver 0.0.0.0:8000\\\"\" > .bash_aliases"
```

# Take snapshot

```
lxc snapshot $NAME base
```

Or set the version at the snapshot name

```
lxc snapshot $NAME 1.7
```

To rebuild a snapshot

```
lxc delete $NAME/base
lxc snapshot $NAME base
```

# Creating a new workspace

Create a new container from a snapshot

```
lxc copy $NAME/base $SITE_NAME
```

Make the container privileged so UID mappings are the same
and so it can write read/write to the workspace

```
lxc config set $SITE_NAME security.privileged true
```

Clone source code into directory

```
git clone $GIT_URL sitesourcecode
```

If you are making a new project create a folder for it

```
mkdir sitesourcecode
```

Setup permissions so that container can write to folder

```
sudo chmod -R go+w $PWD/sitesourcecode
```

Add directory as home

```
lxc config device add $SITE_NAME src disk source=$PWD/sitesourcecode path=/home/ubuntu/src
```

If you are creating a new project get wagtail to build the site

```
lxc exec $SITE_NAME -- su - ubuntu -c "wagtail start mysite /home/ubuntu/src"
```

# Provisioning

I usually put a bash script into the code repository which installs any extra packages
and installs the PIP requirements.

You can also put your bash aliases in here instead of setting in the initial container or to add more.

You could make something similar to the following script in provision.sh

```
sudo apt install rabbitmq-server
pip3 install -r requirements.txt
echo "alias dj=\"python3 $HOME/src/manage.py\"
alias djrun=\"dj runserver 0.0.0.0:8000\"" > .bash_aliases
```

Run the provision script

```
lxc exec $SITE_NAME -- su - ubuntu -c "./src/provision.sh"
```

# Running wagtail

Get a shell for the container

```
lxc start $SITE_NAME
lxc exec $SITE_NAME -- su - ubuntu
```

Check that django and the aliases are working

```
dj
```

Then run the general setup for your site, usually similar to the following

```
dj makemigrations
dj migrate
dj collectstatic
dj compress
dj createsuperuser
```

Then start your site

```
djrun
```

To find out the IP address of the container, using the following command on the host

```
lxc list
```

Once you have found the IP address then use the IP_ADDR:8000 in your web browser


# Container Profile

You can setup a profile for your container, so that it acts in a similar way to your webhost specfications

The following code creates a profile similar to OVH's VPS SSD 1
which has 1 CPU, 2 GB RAM and 100 Mbit network

```
lxc profile create ovh-vps-ssd-1
lxc profile set ovh-vps-ssd-1 limits.cpu 1
lxc profile set ovh-vps-ssd-1 limits.memory 2048MB
```

```
lxc profile device add ovh-vps-ssd-1 eth0 nic name=eth0 nictype=bridged parent=lxdbr0
lxc profile device set ovh-vps-ssd-1 eth0 limits.ingress 100Mbit
lxc profile device set ovh-vps-ssd-1 eth0 limits.egress 100Mbit
```

Then apply this profile to your container

```
lxc profile add $SITE_NAME ovh-vps-ssd-1
```
