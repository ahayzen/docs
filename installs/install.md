
This assumes that you have a base install from ones of the [distro installs](/installs/distro/index.md), eg [Debian](/installs/distro/debian.md) or [Ubuntu](/installs/distro/ubuntu.md)

# LUKS Backup

If you enabled disk encryption backup the LUKS headers, so if they become corrupted one can restore the data.

```
cryptsetup luksHeaderBackup --header-backup-file <file> <device>
cryptsetup luksHeaderBackup --header-backup-file /tmp/luksHeader.backup /dev/sda1
```

For more info use the official [cryptsetup backup and data recovery](https://gitlab.com/cryptsetup/cryptsetup/wikis/FrequentlyAskedQuestions#6-backup-and-data-recovery)

# Firewall Config

Add the custom firewall profile, which allows for mdns, samba, dhcpv6 and udp from a known device by MAC address (eg NAS drive).

```
sudo nmap -sP $IPADDR
echo "<?xml version="1.0" encoding="utf-8"?>
<zone>
  <short>My Home</short>
  <description>For use in home areas. You mostly trust the other computers on networks to not harm your computer. Only selected incoming connections are accepted.</description>
  <service name="mdns"/>
  <service name="samba-client"/>
  <service name="dhcpv6-client"/>
  <rule family="ipv4">
    <source mac="$MAC_ADDR"/>
    <protocol value="udp"/>
    <accept/>
  </rule>
</zone>" | sudo tee /etc/firewalld/zones/my-home.xml
```

```
firewall-cmd --reload
firewall-cmd --set-default-zone=my-home
firewall-cmd --set-log-denied=all
```

# Flatpak

Add flathub

```
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```

## Base Apps

```
flatpak install flathub org.gnome.Calendar org.gnome.Calculator org.gnome.Characters org.gnome.eog org.gnome.Evince org.gnome.Evolution org.gnome.font-viewer org.gnome.gedit org.gnome.Maps org.gnome.Podcasts org.gnome.Rhythmbox3 org.libreoffice.LibreOffice org.remmina.Remmina
```

These flatpak's aren't ready to use yet, due to integration issues.

```
flatpak install flathub org.gnome.clocks org.gnome.Nautilus org.gnome.Totem org.gnome.Weather
```

## Communication

```
flatpak install flathub org.gnome.Fractal io.github.Hexchat org.telegram.desktop
```

## Developer Additions

```
flatpak install flathub com.jetbrains.PyCharm-Community com.visualstudio.code.oss org.gnome.Boxes org.gnome.Builder org.gnome.ghex org.gnome.gitg org.godotengine.Godot
```

## Extra

FIXME: teleport requires firewall tweaks ?
FIXME: piper isn't ready yet

```
flatpak install flathub com.frac_tion.teleport org.freedesktop.Piper
```

## Games

```
flatpak install flathub com.play0ad.zeroad com.valvesoftware.Steam net.supertuxkart.SuperTuxKart org.supertuxproject.SuperTux
```

## Media

```
flatpak install flathub com.spotify.Client io.github.GnomeMpv org.gimp.GIMP org.inkscape.Inkscape org.videolan.VLC
```

# GNOME Config

Nautilus

  * Sort folders before files
  * When launching an exectuable, ask what to do

```
touch ~/Templates/New\ Text\ Document
```
  
Settings

  * Privacy -> Enable Location
  * Details -> Default Applications -> Ensure are correct
  * Devices -> Screen Display -> Enable Night Light
  * Devices -> Mouse & Touchpad -> Enable Tap to click
  * Network -> Change DNS to 9.9.9.9 and 2620:fe::fe
  * Regional & Languages -> Input Sources -> Add -> Japanese (Mozc)

Clocks

  * Set location for shell

Weather

  * Set location for shell

## Dark Theme

Tweaks

  * Change Theme to Adwaita-dark

Firefox

  * Open `about:config`
  * Right click, New -> String
  * Enter the preference name `widget.content.gtk-theme-override`
  * Enter the value `Adwaita:light`
  * Restart firefox

HexChat

  * Open preferences
  * Select Interfaces -> Colours
  * Set Background Colour to `#1E2224`
  * Set Foreground Colour to `#F0F0F0`

# Syncthing

```
mkdir -p ~/config/systemd/user
cp /usr/lib/systemd/user/syncthing.service ~/.config/systemd/user/
systemctl --user enable --now syncthing.service
```

Open http://localhost:8384/ and delete the default sync directory.

# Developer Additions

## Docker Config

Allow usage of the daemon without sudo

```
sudo groupadd docker
sudo usermod -aG docker $USER
```

Trust network access

```
sudo firewall-cmd --permanent --zone=trusted --change-interface=docker0
sudo firewall-cmd --reload
```

Restart the service

```
sudo systemctl restart docker.service
```
