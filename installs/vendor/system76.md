# System76 Stable PPA

System76 have a PPA which has patches for fixes for their machiens that have not been upstreamed yet, eg Wifi, Keyboard key fixes etc

```
sudo apt-add-repository -ys ppa:system76-dev/stable
sudo apt update
sudo apt install -y system76-driver
```

Note on a NVIDIA based system you should use the following.

```
sudo apt install -y system76-driver-nvidia
```
