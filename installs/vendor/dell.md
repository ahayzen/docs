
# Dell PPA

For a xenial (16.04) machine, this source was added to a Dell machine.

```
sudo add-apt-repository deb http://dell.archive.canonical.com/updates/ xenial-dell public
sudo apt update
```

# Dell preinstall

The following packages are installed on a Dell OEM install.

```
dell-e-star
dell-recovery
dell-recovery-casper
dell-super-key
```

# Devices

  * (Dell XPS 13 - 9360)[/installs/devices/dell_9360.md]
