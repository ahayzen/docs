# Printing

Printing works out of the box with driverless printing in CUPS.

# Scanning

Once this is setup simple-scan should be able to work with the scanner.

## Download driver

http://support.brother.com/g/b/downloadtop.aspx?c=eu_ot&lang=en&prod=dcpl8400cdn_us_eu_cn
http://support.brother.com/g/b/downloadlist.aspx?c=eu_ot&lang=en&prod=dcpl8400cdn_us_eu_cn&os=128
http://support.brother.com/g/b/downloadlist.aspx?c=eu_ot&lang=en&prod=dcpl8400cdn_us_eu_cn&os=128#SelectLanguageType-566_0_1
http://support.brother.com/g/b/downloadend.aspx?c=eu_ot&lang=en&prod=dcpl8400cdn_us_eu_cn&os=128&dlid=dlf006645_000&flang=4&type3=566&dlang=true


## Install

```
sudo dpkg --force-all -i brscan4-0.4.5-1.amd64.deb
```

## Configure

If you know the model and IP address add it with.

```
brsaneconfig4 -a name=DCP-L8400CDN model=DCP-L8400CDN ip=$IPADDR
```

This can show the list of supported and network devices found

```
brsaneconfig4 -q
```

This can be used to ping the scanner 

```
brsaneconfig4 -p
```
