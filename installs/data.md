
Backup locations

# Documents

```
~/Documents
~/Projects
```

# Keys

```
~/.gnupg
~/.ssh
```

# Media

```
~/Music
~/Pictures
~/Videos
```

# Network

```
/etc/hosts
/etc/NetworkManager/system-connections
```

When restoring ensure that they have permission 600. 

```
sudo chmod 600 /etc/NetworkManager/system-connections/*
```

# Further

Any bash alias'

```
~/.bash_aliases
```

Any apps that are installed such as firefox, hexchat

```
~/.mozilla
~/.var/app/io.github.HexChat
~/.var/app
```
