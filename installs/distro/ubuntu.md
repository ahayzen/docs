
# Download Ubuntu

Download latest Ubuntu desktop edition from https://www.ubuntu.com/download/desktop

Follow the installation steps, select the following options when available

  * "Encrypt disk"
  * "Minimal Install"

# Base Distro

## Install base packages

```
sudo apt install exfat-fuse exfat-utils flatpak firewall-config gnome-software-plugin-flatpak gnome-user-share ibus-mozc grilo-plugins-0.3-extra tlp tlp-rdw pass ratbagd rygel scdaemon vino
sudo apt install flashplugin-installer libdvd-pkg ubuntu-restricted-extras
sudo apt install amd64-microcode | intel-microcode
```

## Start and configure services

```
sudo systemctl enable --now firewalld
sudo tlp start
sudo dpkg-reconfigure libdvd-pkg
```

## Install GNOME Session

```
sudo apt install gnome-backgrounds gnome-session gnome-shell-extensions gnome-tweaks
```

## Install whitelisted apps (that aren't flatpak yet)

```
sudo apt install baobab cheese gnome-clocks gnome-logs gnome-system-monitor gnome-weather simple-scan syncthing tilix totem
sudo update-alternatives --config x-terminal-emulator
```

## Remove things we don't need

```
sudo apt purge eog evince gedit gnome-font-viewer gnome-software-plugin-snap snapd
sudo apt autoremove
```

# Developer Additions

```
sudo apt install docker.io git network-manager-openvpn-gnome
```

# Fixes

## KVM Permissions

Not sure if libvirt is needed, or these are the full permissions.

```
sudo usermod -aG kvm $USER
sudo usermod -aG libvirt $USER
sudo chown root:kvm /dev/kvm
```
