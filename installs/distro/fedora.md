
FIXME: this is not complete and suffers issues with codecs

# Download Fedora

Download latest Fedora Workstation from https://getfedora.org/en_GB/workstation/download/

Follow the installation steps, select the following options when available

  * "Encrypt my data"

# Base Distro

## Enable free and non-free

Enable free and non-free RPMFusion repositories.

```
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

## Install base packages

ratbagd seems strange
scdaemon - gnupg-pkcs11-scd or gnupg2-smime (installed) might provide smartcard support
libavcodec-extra - missing
libdvdcss - missing
ttf-mscorefonts-installer - missing
flashplugin-installer - missing

```
sudo dnf install exfat-utils fuse-exfat firewall-config tlp tlp-rdw pass libratbag-ratbagd
sudo dnf install gstreamer1-plugins-bad-free-gtk gstreamer1-plugins-bad-freeworld gstreamer1-plugins-bad-nonfree gstreamer1-libav gstreamer1-plugins-ugly gstreamer1-plugins-vaapi ffmpeg libdvdnav libdvdread lsdvd unrar
```

## Start and configure services

FIXME: libdvd-pkg/libdvdcss configuration

```
sudo systemctl enable --now firewalld
sudo tlp start
```

## Install whitelisted apps (that aren't flatpak yet)

```
sudo dnf install gnome-tweaks syncthing tilix
gsettings set org.gnome.desktop.default-applications.terminal exec tilix
```

## Remove things we don't need

```
sudo dnf remove eog evince gedit gnome-boxes gnome-calculator gnome-calendar gnome-contacts gnome-documents gnome-font-viewer gnome-maps gnome-photos libreoffice* rhythmbox
sudo dnf autoremove
```

# Developer Additions

```
sudo dnf install docker
```

