
# Download Debian

Download Debian from https://www.debian.org/distrib/ , I usually use a netinst https://www.debian.org/distrib/netinst

Follow the installation steps, select the following options when available

  * "GNOME" in the tasksel screen
  * Disk Encryption when formatting disks

If you need firmware either

  * Download the firmware from http://cdimage.debian.org/cdimage/unofficial/non-free/firmware/ and put it inside the USB stick in a directory called "firmware" in the root folder.
  * Use a prebundled unofficial image http://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/

# Base Distro

## Enable non-free, contrib and backports

Enable contrib and non-free

```
sudo add-apt-repository contrib
sudo add-apt-repository non-free
```

Enable backports

```
sudo add-apt-repository "deb http://ftp.debian.org/debian stretch-backports main contrib non-free"
```

Update

```
sudo apt update
```

## Install missing low level packages

We use backports to get the latest firmware.

```
sudo apt -t stretch-backports firmware-linux
sudo apt install command-not-found fwupd plymouth plymouth-themes
```

Note that firmware-linux includes CPU microcode.

## Install base packages

FIXME: check if all of these are required
FIXME: flash ?

missing gstreamer1.0-fluendo-mp3 grilo-plugins-0.3-extra

potentially preinstalled (in testing)
exfat-fuse exfat-utils gstreamer1.0-libav gstreamer1.0-plugins-ugly libdvdnav4 libdvdread4

```
sudo apt -t stretch-backports install flatpak
sudo apt install exfat-fuse exfat-utils firewall-config gnome-software-plugin-flatpak ibus-mozc network-manager-config-connectivity-debian tlp tlp-rdw pass ratbagd scdaemon spice-vdagent systemd-coredump
sudo apt install gstreamer1.0-fluendo-mp3 gstreamer1.0-libav gstreamer1.0-plugins-ugly gstreamer1.0-vaapi libavcodec-extra libdvdnav4 libdvdread4 libdvd-pkg ttf-mscorefonts-installer unrar
sudo apt purge kasumi opensc
sudo apt autoremove
```

## Start and configure services

Plymouth config

Might need to enable modeset https://wiki.debian.org/plymouth#Configuration

Update `/etc/default/grub` so that the splash is enabled and the resolution is correct.

```
GRUB_GFXMODE=1920x1080x32
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"
```

Then update grub's config and set the theme to plymouth, note tribar is good for text-only boot.

```
sudo update-grub2
sudo /usr/sbin/plymouth-set-default-theme -R spinner
```

Pick Joy as the theme for desktop and grub

```
sudo update-alternatives --config desktop-theme
sudo update-alternatives --config desktop-grub
sudo update-grub2
```

Simple services

update-command-not-found didn't do anything first time around, maybe needs a reboot?

```
sudo update-command-not-found
sudo systemctl enable --now firewalld
sudo tlp start
sudo dpkg-reconfigure libdvd-pkg
```

## Install whitelisted apps (that aren't flatpak yet)

```
sudo apt install syncthing tilix
sudo update-alternatives --config x-terminal-emulator
```

These are already installed in debian due to GNOME tasksel, will need to remove when flatpaks are available

  * baobab
  * cheese
  * file-roller
  * gnome-clocks
  * gnome-logs
  * gnome-system-monitor
  * gnome-tweak-tool
  * gnome-weather
  * simple-scan
  * totem

## Remove things we don't need

```
sudo apt purge eog evince evolution gedit gimp gnome-calculator gnome-calendar gnome-contacts gnome-documents gnome-games gnome-font-viewer gnome-maps gnome-music gnome-sound-recorder gnome-todo im-config inkscape libreoffice* rhythmbox shotwell transimission-gtk vinagre
sudo apt autoremove
```

# Developer Additions

docker.io isn't in stretch, might need to use https://docs.docker.com/install/linux/docker-ce/debian/

git might already be installed

```
sudo apt install docker.io git network-manager-openvpn-gnome
```

# Fixes

## KVM Permissions

Not sure if libvirt is needed, or these are the full permissions.

```
sudo usermod -aG kvm $USER
sudo usermod -aG libvirt $USER
sudo chown root:kvm /dev/kvm
```
