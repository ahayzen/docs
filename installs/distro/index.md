This document describes the baseline that distro installs in this folder base should meet.

The following distros have met this baseline

  * [Debian](/installs/distro/debian.md)
  * [Fedora](/installs/distro/fedora.md) (incomplete)
  * [Ubuntu](/installs/distro/ubuntu.md)

# Applications and environment

  * flatpak
  * GNOME Shell + extensions
  * GNOME Apps which are not flatpak's
  * pass

## Whitelisted apps until they are flatpak's

  * baobab
  * cheese
  * firefox
  * file-roller
  * gnome-clocks
  * gnome-logs
  * gnome-nettool
  * gnome-system-monitor
  * gnome-tweaks
  * gnome-weather
  * nautilus
  * seahorse
  * simple-scan
  * totem
  * tilix
  * yelp

# Services

  * bluetooth
  * network captive portal
  * command-not-found
  * coredumpctl (or apport on Ubuntu)
  * cups
  * DLNA (eg rygel and grilo)
  * firewalld
  * fwupd
  * Japanese input (mozc)
  * local network shares (eg SMB)
  * LUKS
  * KVM
  * ratbagd (for Piper - mouse configuration)
  * smartcard reader (GPG key)
  * plymouth
  * remote desktop share
  * sane (eg sane-utils for scanning)
  * spice-vdagent (for VMs)
  * syncthing (FIXME: use flatpak now? - no autostart)
  * tlp
  * VNC (eg vino)

# Firmware, codecs and drivers

  * CPU microcode
  * DVD Playback
  * Exfat support
  * Flash Player
  * NTFS support
  * Restricted A/V codecs (eg ubuntu-restricted-extras)

# Developer machines

  * docker
  * git
  * openVPN

# FIXME: Questions?

  * build-essential
  * cmake
  * dia
  * iotop
  * soundconverter
  * soundrecorder?
  
# Raw packages

TODO: This is a list of the raw packages a (debian-like) distro should have.

Codecs

```

```

GNOME

```

```

GNOME Apps

```

```

Services

```

```

System

```

```
