
# Welcome to my documentation page!

Please note that these docs are written in the hope that they are useful.

The documentation is split into sections

# Applications

How to setup applications that are more complicated than apt install or snap install. [Link](/applications)

# Installs

Which applications, data and services I add to specific installs, eg for dev machines or media etc. [Link](/installs)

# Services

How to setup services that are more complicated than apt install or snap install. [Link](/services)

# Ubuntu

Guides for branching, building and debugging various Ubuntu related packages. [Link](/ubuntu)

# Virtual

How to setup virtual or container environments in general and for specific tasks. [Link](/virtual)
