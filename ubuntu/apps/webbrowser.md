
# Source

```
bzr branch lp:webbrowser-app/staging
```

# Building

## Prerequisites

```
sudo apt build-dep webbrowser-app
```

## Compiling

```
cmake .
make -j4
```

## Debian

```
debuild -uc -us -b
```

# Running

```
./src/app/webbrowser/webbrowser-app
```

# Tests

```
ctest -V
```

## Running tests visually

```
./tests/unittests/qml/tst_QmlTests -input tests/unittests/qml -import src
```

## Running a single test visually

```
./tests/unittests/qml/tst_QmlTests -input tests/unittests/qml/tst_AddressBar.qml -import src AddressBar::test_actionButtonShouldBeDisabledWhenEmpty
```

## Running a test slowly

```
./tests/unittests/qml/tst_QmlTests -input tests/unittests/qml/tst_AddressBar.qml -import src AddressBar::test_actionButtonShouldBeDisabledWhenEmpty -eventdelay 1
```
