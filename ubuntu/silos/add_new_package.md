# Create a project

Create a project on launchpad and push your code to trunk.

```
bzr init
bzr push lp:$PROJECT
```

# Create an empty merge proposal

Branch the code

```
bzr branch lp:$PROJECT project
cd project
```

Perform an empty commit

```
bzr commit --unchanged
```

Push the code to a branch

```
bzr push lp:$BRANCH
```

Propose the branch into trunk

# Add to bileto

Add the merge proposal url to the list of merge requests and then select build
