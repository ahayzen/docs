# Set Silo Number

```
export SILO_NUMBER=001
```

# Add Pinning

```
echo """
Package: *
Pin: release o=LP-PPA-ci-train-ppa-service-$SILO_NUMBER
Pin-Priority: 1002
""" | sudo tee -a /etc/apt/preferences.d/extra-ppas.pref
```

# Add PPA

```
sudo add-apt-repository ppa:ci-train-ppa-service/ubuntu/$SILO_NUMBER
```

# Update APT

```
sudo apt update
```

# Install Relevant Packages

```
sudo apt install relevant-packages
```

