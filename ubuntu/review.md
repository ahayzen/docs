# Code

## Arithmatic

 * Ensure that divisions can never have a zero on the base, eg ```2 / 0```

## Control Flows

 * Check boundary conditions
 * Prefer if statements to have an else condition
 * Ensure switch cases have a break and a default

## Exceptions

 * Ensure that the except case of a try catch does not silently fail

## Formatting

 * Don't Repeat Yourself (DRY)
 * New line at the end of all files
 * Methods and properties are ordered alphabetically

## Imports

 * Check imports use the latest version and are consistent across all files
 * Check all imports are used
 * [cpp] Check fully qualified includes are used eg ```#include <QtCore/QString>```

## Loops

 * Attempt not to use break or continue
 * Loops must have a fixed upper bound

## Methods

 * Prefer only one return method
 * Check if any methods can be rewritten in a reusable way
 * Caller should check any return value

## Variables

 * Check all variables are used
 * Smallest level of scope

# New contributor

 * Ensure that if this a new contributor they have been added to AUTHORS

# Packaging

 * Ensure the changelog has been updated
 * Ensure any fixed bugs are linked in the changelog

# Tests

 * Ensure all tests pass
 * If fixing a bug, ensure there is a regression test

# Tools

## Python

 * Passes PEP8
 * Passes pyflakes

# Translations

 * If there are new or changes to the UI strings ensure the .pot has been updated
