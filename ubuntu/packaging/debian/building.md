# Building the deb

Ensure you are in the root of the repo

```
dpkg-buildpackage -tc
```

If you don't need to sign the package

```
dpkg-buildpackage -tc -us -uc
```

# Updating

If you have made changes and want to build the deb

```
dpkg-source --commit
```

Then enter the patch name and build as normal

# Clean environment

For building in a clean environment see [pbuilder](/ubuntu/packaging/debian/pbuilder.md)
