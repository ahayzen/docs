pbuilder is useful for building debs in a clean environment to ensure that you have the correct build depends etc.

# Prerequisites

```
sudo apt install pbuilder
```

# Create the chroot

```
sudo pbuilder create --debootstrapopts --variant=buildd
```

If you want a different distro release use

```
sudo pbuilder create --debootstrapopts --variant=buildd --distribution bionic
```

# Enabling universe

Add universe to the chroot sources

```
echo """COMPONENTS="main restricted universe multiverse"
if [ "$DIST" == "squeeze" ]; then
    echo "Using a Debian pbuilder environment because DIST is $DIST"
    COMPONENTS="main contrib non-free"
fi""" | tee -a ~/.pbuilderrc
```

Update the chroot

```
sudo pbuilder update --override-config
```

# Adding a PPA

Add the folllowing code to your pbuilder configuration to add the stable-phone-overlay PPA.

```
ALLOWUNTRUSTED=yes
OTHERMIRROR="deb http://ppa.launchpad.net/ci-train-ppa-service/stable-phone-overlay/ubuntu xenial main"
```

Note: Currently setting untrusted to true, should instead use APTKEYRINGS or DEBOOTSTRAPOPTS ?

# Building

```
sudo pbuilder --build *.dsc
```

If you are using a different distro use

```
sudo pbuilder --build *.dsc --distribution bionic
```

## Untrusted packages

If you are using PPAs or have untrusted packages use the following build command.

```
sudo pbuilder --allow-untrusted --build *.dsc
```

# Missing packages

If you know the file that is missing but not the package look at [Package for file](/ubuntu/packaging/debian/package_for_file.md)
