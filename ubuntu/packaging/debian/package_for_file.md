# Install

```
sudo apt install apt-file
apt-file update
```

# Search

The following command lists the packages that contain the file

```
apt-file search $FILE
```
