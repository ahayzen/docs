# Prerequisites

```
sudo apt install dh-make
```

# Branch and prepare the code

Branch the code into a folder named name-version, then ensure a MakeFile exists by running cmake, qmake etc.

```
bzr branch $SOURCE $NAME-$VERSION
qmake .
```

# Adding the debian folder

```
dh_make -c gpl3 -e $EMAIL --createorig
```

# Cleaning the folder

```
cd debian
rm *.ex *.EX
```

If you don't need any docs

```
rm REAME.* *.docs
```

# Edit the files

Ensure that the following files are correct

  * changelog
  * control
  * copyright

# Making a .deb

For building the package into a deb see [building](/ubuntu/packaging/debian/building)


