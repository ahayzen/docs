# Windowed Mode

```
gsettings set com.canonical.Unity8 usage-mode Windowed
```

# Staged Mode

```
gsettings set com.canonical.Unity8 usage-mode Staged
```

