# Install Prerequisites

```
sudo apt install debootstrap schroot
```

# Install

```
sudo debootstrap --variant=buildd --arch armhf --keyring=/usr/share/keyrings/ubuntu-archive-keyring.gpg vivid /home/phablet/build/
```

# Config

```
sudo mkdir /home/phablet/build/home/phablet
sudo cp /etc/apt/sources.list /home/phablet/build/etc/apt/
sudo cp -r /etc/apt/sources.list.d/ /home/phablet/build/etc/apt/
```

```
echo "[build]
description=vivid+overlay
directory=/home/phablet/build
groups=sbuild,root" | sudo tee -a /etc/schroot/schroot.conf
```

# Running

```
sudo mount -o bind /home/phablet /home/phablet/build/home/phablet/
sudo mount -o bind /proc /home/phablet/build/proc/
```

```
sudo schroot -c build
```

