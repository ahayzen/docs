# Profiles

Profiles are stored in the following location

```
/etc/apparmor.d/
```

# Reparse

With the following command you can reparse an apparmor profile that has been manually edited.

```
sudo apparmor_parser -r /etc/apparmor.d/myprofile
```

# Converting a manifest to a profile

The following command converts a given manifest file to a profile, which can then be installed in the profile location above.

```
aa-easyprof --no-verify --manifest=my-apparmor.manifest > myprofile
```

The manifest may look something like the following

```
{
  "profiles": {
    "my-app": {
      "binary": "/usr/bin/my-app",
      "profile_name": "my-app",
      "policy_vendor": "ubuntu",
      "policy_version": 1.3,
      "policy_groups": [
        "audio",
        "networking",
      ],
      "abstractions": [
        "user-tmp"
      ],
      "template_variables": {
        "APP_ID_DBUS": "my_2dapp",
        "APP_PKGNAME_DBUS": "my_2dapp",
        "APP_PKGNAME": "my-app",
        "CLICK_DIR": "/usr/share/my-app"
      },
      "read_path": [
        "/usr/share/applications/",
        "@{HOME}/.local/share/applications/",
      ],
      "write_path": [
        "@{HOME}/@{XDG_DOWNLOAD_DIR}/"
      ]
    }
  }
}
```
