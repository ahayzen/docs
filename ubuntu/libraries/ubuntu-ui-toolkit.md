# Install Prerequisites

```
sudo apt-get build-dep ubuntu-ui-toolkit
```

# Branch the code

```
bzr branch lp:ubuntu-ui-toolkit
cd ubuntu-ui-toolkit
```

# Building

```
qmake -qt5
make
```

# Running an app with the Ubuntu UI Toolkit

```
source export_modules_dir.sh
```

Then within the same terminal start the application.

```
qmlscene /path/to/app.qml
```

