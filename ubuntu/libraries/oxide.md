# Prerequisites

```
sudo apt-get install cmake gettext qt5-qmake qt5-default qtdeclarative5-dev qtbase5-private-dev qtdeclarative5-private-dev qtpositioning5-dev qtfeedback5-dev make ninja-build python g++ pkg-config libglib2.0-dev libgtk2.0-dev libgconf2-dev libcups2-dev libexif-dev libgnome-keyring-dev libnss3-dev libpci-dev libgcrypt-dev libdbus-1-dev libudev-dev libpulse-dev libasound2-dev libx11-dev libxi-dev libcap-dev libfontconfig1-dev libfreetype6-dev libpango1.0-dev libxext-dev libxcomposite-dev libxrender-dev libxcursor-dev libxrandr-dev libxtst-dev libxdamage-dev libxfixes-dev libkrb5-dev libssl-dev libgdk-pixbuf2.0-dev libnotify-dev libffi-dev libandroid-properties-dev libhybris-dev python-psutil gperf bison git subversion
```

Sometimes media-hub fails to build, if you have libmedia-hub-dev installed you can remove and rerun cmake

# Branching

```
git clone https://git.launchpad.net/~oxide-developers/oxide/+git/depot_tools depot_tools
git clone https://git.launchpad.net/~oxide-developers/oxide/+git/oxide-tools oxide-tools
```

Environment

```
export PATH=$PWD/depot_tools:$PWD/oxide-tools:$PATH
```

Branching from Launchpad, where <user-id> is your Launchpad id.

```
fetch_oxide --user-id <user_id> master
```

# Building

```
mkdir src/objdir && cd src/objdir && cmake ../ && make
```

# Running

To use the built version of Oxide with the webbrowser run the following.

```
./src/objdir/out/bin/run_qmlapp.sh /path/to/webbrowser-app
```

# References

  * [Ubuntu Wiki - Oxide Build Instructions](http://wiki.ubuntu.com/Oxide/BuildInstructions)
  * [Ubuntu Wiki - Oxide Get the Code](http://wiki.ubuntu.com/Oxide/GetTheCode)
