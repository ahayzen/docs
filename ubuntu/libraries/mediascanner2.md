# Disable mediascanner2

Modify the file /usr/share/upstart/sessions/mediascanner-2.0.conf so that the following lines are all commented with a "#" at the start

```
start on started dbus
respawn
ubuntu exec mediascanner-service-2.0
```

# Prevent scanning in a directory

Add a file called ".nomedia" to the directory you wish to skip

# Mediascanner Database

The mediascanner database is located at ~/.cache/mediascanner-2.0/mediastore.db

# Running on desktop

Mediascanner2 needs an env var set to run from Ubuntu 17.04.

```
MEDIASCANNER_RUN=1 mediascanner-service-2.0
```
