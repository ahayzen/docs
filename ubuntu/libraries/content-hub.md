
# Peers and rescanning

The json files defining if an app is a source or destination are located in the following directory and have the same name as their app, eg webbrowser-app

```
/usr/share/content-hub/peers/
```

To manually tell content-hub to reload the peers, eg when using debs, use the following command

```
/usr/lib/x86_64-linux-gnu/content-hub/content-hub-peer-hook-wrapper
```

# Running in debug mode

```
CONTENT_HUB_LOGGING_LEVEL=2 content-hub-service
```
