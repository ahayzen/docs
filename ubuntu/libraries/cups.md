
# Adding fake printer

Open system-config-printer from the system settings.

Then select "Add"

Expand "Network printer"

Select "LPD/LPR Host or Printer"

Enter "127.0.0.1" as the "Host"

Select "Forward"

If you have a PPD file locally, select "Provide PPD File". [OpenPrinting Printer List](http://www.linuxprinting.org/printer_list.cgi)

Otherwise select "Select printer from Database"

I selected "HP" as the make and "DeskJet 400" as the model and then the recommended driver.

Then select "Forward", choose a name and description.

Then "Apply", now you should have a fake printer in CUPS that you can use 
