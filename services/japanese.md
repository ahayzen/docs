
# Anthy Text Input

```
sudo apt install anthy ibus-anthy
```

  * Restart your session
  * System Settings -> Regional & Languages -> Input Sources -> Add -> Japanese (Anthy) (IBus)

Note in GNOME Shell you have to click "Other" from the sources list to find non-english.


# Language packs

System Settings -> Language Support -> Add -> Tick Japanese

