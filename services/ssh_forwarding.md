
# Remote forwarding 

The following command will make the port 2000 on the localhost accessible on the port 1000 on the example.com.

Note that -nNT prevent the allocation of tty and will just perform port forwarding.

```
ssh -nNT -R 1000:localhost:2000 user@example.com
```

## Allow the remote host to forward

On the example.com run the following to add "GatewayPorts yes" to the sshd config.

```
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig
echo "GatewayPorts yes" | sudo tee -a /etc/ssh/sshd_config
sudo service ssh restart
```

# Local forwarding

The following command will make the local port 9000 on localhost return port 80 from google.com by using an encrypted connection via example.com

```
ssh -L 9000:google.com:80 user@example.com
```


# Advanced

## Using Server keep alive

Note this is an encrypted packet that cannot be spoofed, unlike tcp keep alive.

```
-o ServerAliveInterval=60 -o ServerAliveCountMax=3
```

## Using a master socket

### Starting

```
ssh -S <socket-name> -fnNTM -R <remote-port>:<local-ip>:<local-port> <user>@<remote-ip>
```

### Status

```
ssh -S <socket-name> -O check <user>@<remote-ip>
```

### Stop

```
ssh -S <socket-name> -O stop <user>@<remote-ip>
```
