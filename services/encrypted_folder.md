# Install

```
sudo apt install ecryptfs-utils
```

# Setup

```
mkdir encr
chmod 700 encr
```

# Mount

```
sudo mount -t ecryptfs encr/ encr/
```

# Unmount

```
sudo umount encr/
```

# References

  * [https://help.ubuntu.com/lts/serverguide/ecryptfs.html](https://help.ubuntu.com/lts/serverguide/ecryptfs.html)
  * [https://opensourcehacker.com/2011/04/15/encrypted-folders-on-ubuntu-linux-using-ecryptfs-on-an-external-hard-drive/](https://opensourcehacker.com/2011/04/15/encrypted-folders-on-ubuntu-linux-using-ecryptfs-on-an-external-hard-drive/)
