# Install

```
sudo apt install zfsutils-linux
```

If ZFS module isn't loaded automatically use

```
sudo modprobe zfs
```

# Info

```
zpool list
zpool status
zfs list
```

# Add space to pool

```
truncate -s 10G /path/to/file.img
zpool add $POOL /path/to/file.img
```
