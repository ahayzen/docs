This installation guide assumes you have run the following installs beforehand.

  * [Ubuntu Make](/applications/development/ubuntu-make.md)

# Install

```
umake ide atom
```

# Config

Enable the following settings

Editor
  * Show indent guide
  * Show invisibles
  * Tab Length = 4

# Plugins

  * minimap
  * minimap-cursorline
  * minimap-find-and-replace
  * minimap-selection

  * zen
    * Show minimap in settings
