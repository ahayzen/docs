
This installation guide assumes you have run the following installs beforehand.

  * [Ubuntu Make](/applications/development/ubuntu-make.md)

# Install

```
umake ide pycharm
```

# Config

Choose 'Darcula' as the theme

