
This installation guide assumes you have run the following installs beforehand.

  * [Ubuntu Make](/applications/development/ubuntu-make.md)

# Install

```
umake rust
```

Ensure that you logout and login

# Commands

The following commands should now be available

```
cargo
rustc
rustdoc
rust-gdb
```

# Creating a project

```
cargo new $PROJECT --bin
```

Or for a library

```
cargo new $PROJECT --lib
```

# Compiling and running

## Debug

```
cargo build
cargo run
```

## Release

```
cargo build --release
cargo run --release
```

## Manually

```
rustc main.rs
```

# Atom integration

This assumes that Atom is already installed

  * [Atom Installation](/applications/development/atom.md)

## language-rust

Install the following package in atom

```
language-rust
```

## Racer

Install racer within rust

```
cargo install racer
```

Download the source of rust from [https://www.rust-lang.org/install.html](https://www.rust-lang.org/install.html) and extract into $HOME/.rustsrc

Install the following package in atom

```
racer
```

Set the path to racer bin to the following

```
$HOME/.cargo/bin/racer
```

Set the rust src path to the following

```
$HOME/.rustsrc/*/src
```

To stop flickering in project tree, in the settings of tree-view check "Hide Ignored Names"
