# Install

Install PPA as package in archive is too old

```
sudo add-apt-repository ppa:ubuntu-desktop/ubuntu-make
sudo apt update
sudo apt install ubuntu-make
```

# Usage

Then you can install IDEs easily by using umake

```
umake ide-name
```

Then accept the installation path

# Removing an IDE

```
umake -r ide-name
```


