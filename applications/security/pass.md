
# Install

```
sudo apt install pass
```

# Creation

```
pass init $GPG_ID
pass git init
pass git remote add origin $GIT_URL
```

# Restore from branch

```
git clone $GIT_URL ~/.password-store
```

# Listing keys

```
pass
```

# Adding keys

```
pass insert internet/store
```

For multiline entries, such as meta

```
pass insert -m internet/store.meta
```

# Generating

```
pass generate internet/bank 15
```

Also with no symbols

```
pass generate internet/bank 10 --no-symbols
```

# Retrieving keys

```
pass internet/store
```

# Removing keys

```
pass rm internet/store
```

