
# Installation

```
sudo apt install pinentry-qt scdaemon yubikey-personalization-gui 
```

# Importing public key

```
gpg --import < publickey.txt
```

With the yubikey attached check it finds it
pass uses gpg2 so ensure gpg2 works as well.

Note that this steps seems to link the secrets on the card to device, so is required.

```
gpg --card-status
gpg2 --card-status
```

Should see sec# and ssb>

```
gpg --list-secret-keys $KEY-ID
```

Mark as trusted

```
gpg --edit-key $KEY-ID
gpg> trust
Your decision? 5 - I trust ultimately
```
