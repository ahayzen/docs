
# HexChat

## Install

```
sudo apt install hexchat
```

For an indicator in the messaging menu install the following package

```
sudo apt install hexchat-indicator
```


## Config

Copy the following folder

```
~/.config/hexchat/
```

